# 部署说明

## 自动部署

CentOS 7 环境中执行以下命令:

`sh <(curl -sL https://git.io/blog-install)`

## 手动部署

手动部署请参考: [CentOS 手动部署方案](https://github.com/ermaozi/blog/wiki/CentOS%E6%89%8B%E5%8A%A8%E9%83%A8%E7%BD%B2%E6%96%B9%E6%A1%88)
